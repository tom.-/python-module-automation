# Install and configure required modules

- name: Ensure Python is installed
  hosts: localhost
  become: True
  vars:
    python_version: python3.8
  tasks:
    - name: gather package facts
      package_facts:
        manager: auto

    - yum:
        name: "{{ python_version }}"
        state: present
      when: "python_version not in ansible_facts.packages and (ansible_distribution == 'CentOS' or ansible_distribution == 'RedHat')"

    - apt:
        name: "{{ python_version }}"
        state: present
      when: "python_version not in ansible_facts.packages and ansible_distribution == 'Ubuntu'"

- name: install and configure modules
  hosts: localhost
  become: True
  vars_files: 
    - vars/pip_modules_vars.yml
    - vars/jupyterhub_users.yml
  tasks:
    - name: Install pip modules
      pip:
        name: "{{ item }}"
        executable: /home/tom/anaconda3/bin/pip
        state: latest
        # Additional options to include for individual deployments...
        # requirements: <value>
        # virtualenv: <value>
      loop: "{{ modules_to_install }}"

    - name: Install npm dependencies
      npm:
        name: "{{ item }}"
        global: True
        state: present
      loop: "{{ npm_dependencies }}"
    
    - name: Create Jupyterhub users
      user:
        name: "{{ item }}"
        state: present
      loop: "{{ jupyterhub_users }}"

    - name: Configure firewall for Jupyterhub
      firewalld:
        port: "{{ item }}"
        permanent: yes
        state: enabled
      loop: "{{ required_firewall_ports }}"
      notify: restart_firewalld
  
    - name: Set up JupyterHub as systemd service
      template:
        src: templates/jupyterhub.service.j2
        dest: /lib/systemd/system/jupyterhub.service
      notify: enable_jupyterhub_service

  handlers:
    - name: enable_jupyterhub_service
      systemd:
        name: jupyterhub
        daemon_reload: True
        state: started
        enabled: True

    - name: restart_firewalld
      service:
        name: firewalld
        state: restarted
